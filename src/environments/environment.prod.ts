export const environment = {
  production: true,
  apiUrl: process.env['NG_APP_API_URL'],
  hasuraAdminSecret: process.env['NG_APP_HASURA_ADMIN_SECRET'],
};
// if (typeof process !== 'undefined' && process.env) {
//   environment.apiUrl = process.env['API_URL'] || 'default-api-url';
//   environment.hasuraAdminSecret = process.env['HASURA_ADMIN_SECRET'] || 'default-admin-secret';
// }