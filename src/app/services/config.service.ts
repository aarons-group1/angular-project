// config.service.ts
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'

console.log("environment apiUrl: ", environment.apiUrl);
@Injectable({
  providedIn: 'root',
})


export class ConfigService {
  get apiUrl(): string {
    return environment.apiUrl;
  }

  get hasuraAdminSecret(): string {
    return environment.hasuraAdminSecret;
  }
}
