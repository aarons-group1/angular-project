// Create a new file, e.g., my-api.service.ts

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class MyApiService {
  private apiUrl = '/.netlify/functions/myFunction'; // Adjust the path accordingly

  constructor(private http: HttpClient) {}

  getApiData() {
    return this.http.get(this.apiUrl);
  }
}
